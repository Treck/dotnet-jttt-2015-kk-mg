namespace JTTT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Waruneks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        adresURL = c.String(),
                        poszukiwaneSlowo = c.String(),
                        email = c.String(),
                        ListaZadan_listaZadanId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ListaZadans", t => t.ListaZadan_listaZadanId)
                .Index(t => t.ListaZadan_listaZadanId);
            
            CreateTable(
                "dbo.ListaZadans",
                c => new
                    {
                        listaZadanId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.listaZadanId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Waruneks", "ListaZadan_listaZadanId", "dbo.ListaZadans");
            DropIndex("dbo.Waruneks", new[] { "ListaZadan_listaZadanId" });
            DropTable("dbo.ListaZadans");
            DropTable("dbo.Waruneks");
        }
    }
}
