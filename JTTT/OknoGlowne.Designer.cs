﻿namespace JTTT
{
    partial class OknoGlowne
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.urlTextBox = new System.Windows.Forms.TextBox();
            this.frazaTextBox = new System.Windows.Forms.TextBox();
            this.mailTextBox = new System.Windows.Forms.TextBox();
            this.wykonajButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.ListBox = new System.Windows.Forms.ListBox();
            this.ZaladujButton = new System.Windows.Forms.Button();
            this.deserializacjaButton = new System.Windows.Forms.Button();
            this.serializeButton = new System.Windows.Forms.Button();
            this.czyscButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.purgeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // urlTextBox
            // 
            this.urlTextBox.Location = new System.Drawing.Point(92, 9);
            this.urlTextBox.Name = "urlTextBox";
            this.urlTextBox.Size = new System.Drawing.Size(156, 20);
            this.urlTextBox.TabIndex = 0;
            // 
            // frazaTextBox
            // 
            this.frazaTextBox.Location = new System.Drawing.Point(92, 35);
            this.frazaTextBox.Name = "frazaTextBox";
            this.frazaTextBox.Size = new System.Drawing.Size(156, 20);
            this.frazaTextBox.TabIndex = 1;
            // 
            // mailTextBox
            // 
            this.mailTextBox.Location = new System.Drawing.Point(92, 61);
            this.mailTextBox.Name = "mailTextBox";
            this.mailTextBox.Size = new System.Drawing.Size(156, 20);
            this.mailTextBox.TabIndex = 2;
            // 
            // wykonajButton
            // 
            this.wykonajButton.Location = new System.Drawing.Point(271, 144);
            this.wykonajButton.Name = "wykonajButton";
            this.wykonajButton.Size = new System.Drawing.Size(75, 23);
            this.wykonajButton.TabIndex = 3;
            this.wykonajButton.Text = "Wykonaj";
            this.wykonajButton.UseVisualStyleBackColor = true;
            this.wykonajButton.Click += new System.EventHandler(this.wykonajButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(721, 146);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 4;
            this.exitButton.Text = "Wyjdź";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // ListBox
            // 
            this.ListBox.FormattingEnabled = true;
            this.ListBox.Location = new System.Drawing.Point(271, 9);
            this.ListBox.Name = "ListBox";
            this.ListBox.Size = new System.Drawing.Size(531, 95);
            this.ListBox.TabIndex = 5;
            // 
            // ZaladujButton
            // 
            this.ZaladujButton.Location = new System.Drawing.Point(92, 100);
            this.ZaladujButton.Name = "ZaladujButton";
            this.ZaladujButton.Size = new System.Drawing.Size(81, 23);
            this.ZaladujButton.TabIndex = 6;
            this.ZaladujButton.Text = "Dodaj";
            this.ZaladujButton.UseVisualStyleBackColor = true;
            this.ZaladujButton.Click += new System.EventHandler(this.ZaladujButton_Click);
            // 
            // deserializacjaButton
            // 
            this.deserializacjaButton.Location = new System.Drawing.Point(721, 110);
            this.deserializacjaButton.Name = "deserializacjaButton";
            this.deserializacjaButton.Size = new System.Drawing.Size(81, 23);
            this.deserializacjaButton.TabIndex = 7;
            this.deserializacjaButton.Text = "Deserializacja";
            this.deserializacjaButton.UseVisualStyleBackColor = true;
            this.deserializacjaButton.Click += new System.EventHandler(this.deserializacjaButton_Click);
            // 
            // serializeButton
            // 
            this.serializeButton.Location = new System.Drawing.Point(271, 110);
            this.serializeButton.Name = "serializeButton";
            this.serializeButton.Size = new System.Drawing.Size(81, 23);
            this.serializeButton.TabIndex = 8;
            this.serializeButton.Text = "Serializacja";
            this.serializeButton.UseVisualStyleBackColor = true;
            this.serializeButton.Click += new System.EventHandler(this.serializeButton_Click);
            // 
            // czyscButton
            // 
            this.czyscButton.Location = new System.Drawing.Point(92, 144);
            this.czyscButton.Name = "czyscButton";
            this.czyscButton.Size = new System.Drawing.Size(81, 23);
            this.czyscButton.TabIndex = 9;
            this.czyscButton.Text = "Czyść";
            this.czyscButton.UseVisualStyleBackColor = true;
            this.czyscButton.Click += new System.EventHandler(this.czyscButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Nazwa strony:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Nazwa frazy:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Email:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // purgeButton
            // 
            this.purgeButton.Location = new System.Drawing.Point(358, 110);
            this.purgeButton.Name = "purgeButton";
            this.purgeButton.Size = new System.Drawing.Size(75, 23);
            this.purgeButton.TabIndex = 13;
            this.purgeButton.Text = "Purge";
            this.purgeButton.UseVisualStyleBackColor = true;
            this.purgeButton.Click += new System.EventHandler(this.purgeButton_Click);
            // 
            // OknoGlowne
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 181);
            this.Controls.Add(this.purgeButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.czyscButton);
            this.Controls.Add(this.serializeButton);
            this.Controls.Add(this.deserializacjaButton);
            this.Controls.Add(this.ZaladujButton);
            this.Controls.Add(this.ListBox);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.wykonajButton);
            this.Controls.Add(this.mailTextBox);
            this.Controls.Add(this.frazaTextBox);
            this.Controls.Add(this.urlTextBox);
            this.Name = "OknoGlowne";
            this.Text = "JTTT v2.0";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox urlTextBox;
        private System.Windows.Forms.TextBox frazaTextBox;
        private System.Windows.Forms.TextBox mailTextBox;
        private System.Windows.Forms.Button wykonajButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.ListBox ListBox;
        private System.Windows.Forms.Button ZaladujButton;
        private System.Windows.Forms.Button deserializacjaButton;
        private System.Windows.Forms.Button serializeButton;
        private System.Windows.Forms.Button czyscButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button purgeButton;
    }
}

