﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace JTTT
{
    public partial class OknoGlowne : Form
    {
        public ListaZadan Obiekt = new ListaZadan();
        public Log Dziennik = new Log();

        public OknoGlowne()
        {
            InitializeComponent();
            ListBox.DataSource = Obiekt.ListaWarunkow;
            Dziennik.NowyWpis("Utworzenie obiektu.");
        }

        public void wykonajButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Obiekt.ListaWarunkow.Count() ; i++ )
            {
                SkrzynkaPocztowa klientPoczty = new SkrzynkaPocztowa(Obiekt.ListaWarunkow[i].email);
                BindingList<String> AdresyObrazkow = new BindingList<string>();
                AdresyObrazkow = Obiekt.ListaWarunkow[i].ZdobadzObrazki();

                for (int j = 0; j < AdresyObrazkow.Count; j++)
                {
                    Dziennik.NowyWpis("Znaleziono nowy plik.\n");
                    klientPoczty.lokalizacjaZalacznika = AdresyObrazkow[j];
                    klientPoczty.WysylanieWiadomosci();
                    Obiekt.ListaWarunkow[j].UsunObrazkiZDysku(AdresyObrazkow[j]);
                    Dziennik.NowyWpis("Wysłano plik: " + AdresyObrazkow[j] + ".\n");
                }
                
            }
            
        }
        
        public void exitButton_Click(object sender, EventArgs e)
        {
            Dziennik.NowyWpis("Poprawne zamknięcie aplikacji.");
            this.Close(); 
        }

        public void ZaladujButton_Click(object sender, EventArgs e)
        {
            Warunek NewObject = new Warunek(urlTextBox.Text, frazaTextBox.Text, mailTextBox.Text);
            Obiekt.ListaWarunkow.Add(NewObject);
            AktualizujListBox();
            Dziennik.NowyWpis("Dodano \"" + NewObject.poszukiwaneSlowo + "\" do listy.");
            
        }
        
        public void serializeButton_Click(object sender, EventArgs e)
        {
            Obiekt.ZapisDoBazy();
            Dziennik.NowyWpis("Pomyślnie zapisano dane do pliku.");
        }

        private void deserializacjaButton_Click(object sender, EventArgs e)
        {
            Obiekt.OdczytZBazy();
            AktualizujListBox();
            Dziennik.NowyWpis("Pomyślnie załadowano dane z pliku.");
        }

        private void czyscButton_Click(object sender, EventArgs e)
        {  
            Obiekt.CzyscListe();
            AktualizujListBox();
        }
        
        private void purgeButton_Click(object sender, EventArgs e)
        {
            Obiekt.CzyscBaze();
            AktualizujListBox();
        }

        public void AktualizujListBox()
        {
            ListBox.DataSource = null;
            ListBox.DataSource = Obiekt.ListaWarunkow;
        }

        
    }
}
