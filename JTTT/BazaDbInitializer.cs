﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class BazaDbInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<BazaDbContext>
    {
        protected override void Seed(BazaDbContext ctx)
        {
            ListaZadan lokalnaLista = new ListaZadan();
            Warunek lokalnyWarunek = new Warunek("", "", "");
            lokalnaLista.ListaWarunkow.Add(lokalnyWarunek);

            ctx.SaveChanges();
            
            base.Seed(ctx);
        }
    }
}
