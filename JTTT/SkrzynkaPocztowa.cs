﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    class SkrzynkaPocztowa
    {
        private string adresEmailOdbiorcy { get; set; }
        private string adresEmailNadawcy { get; set; }
        private string adresURL { get; set; }
        public string lokalizacjaZalacznika { get; set; }
        private string tematWiadomosci { get; set; }
        private string hasloEmailNadawcy { get; set; }

        //private string od_mail2 = "grzeczny.student.z.ladna.nazwa@gmail.com";
        //private string haslo2 = "2356h24y5u6753uyhu53y365y3ty";

        public SkrzynkaPocztowa(string _adresEmailOdbiorcy = "", 
                                string _adresURL = "", 
                                string _lokalizacjaZalacznika = "",
                                string _adresEmailNadawcy = "dupek153@gmail.com",
                                string _tematWiadomosci = "Obrazki",
                                string _hasloEmailNadawcy = "5hgwhds2")
        {
            adresEmailOdbiorcy = _adresEmailOdbiorcy;
            adresEmailNadawcy = _adresEmailNadawcy;
            adresURL = _adresURL;
            lokalizacjaZalacznika = _lokalizacjaZalacznika;
            tematWiadomosci = _tematWiadomosci;
            hasloEmailNadawcy = _hasloEmailNadawcy;
        }


        public MailMessage TworzenieWiadomosci()
        {
            MailMessage wiadomosc = new MailMessage();
            wiadomosc.From = new MailAddress(adresEmailNadawcy);
            wiadomosc.To.Add(adresEmailOdbiorcy);
            wiadomosc.Subject = tematWiadomosci;
            wiadomosc.IsBodyHtml = true;
            return wiadomosc;
        }

        public SmtpClient UstawianieSerwera()
        {
            SmtpClient serwerPoczty = new SmtpClient("smtp.gmail.com");
            serwerPoczty.Port = 587;
            serwerPoczty.Credentials = new System.Net.NetworkCredential(adresEmailNadawcy, hasloEmailNadawcy);
            serwerPoczty.EnableSsl = true;
            return serwerPoczty;
        }

        public bool DodajObrazekJakoWiadomosc(MailMessage wiadomosc)
        {
            wiadomosc.Body += "<img src=" + adresURL + ">";
            return true;
        }

        public bool DodajObrazekJakoZalacznik(MailMessage wiadomosc, string sciezkaDoObrazka)
        {
            System.Net.Mail.Attachment zalacznik;
            zalacznik = new System.Net.Mail.Attachment(sciezkaDoObrazka);
            wiadomosc.Attachments.Add(zalacznik);

            return true;
        }

        public bool WysylanieWiadomosci()
        {
            MailMessage wiadomosc = new MailMessage();
            SmtpClient serwerPoczty = new SmtpClient();

            wiadomosc = TworzenieWiadomosci();
            serwerPoczty = UstawianieSerwera();
            DodajObrazekJakoZalacznik(wiadomosc, lokalizacjaZalacznika);
            serwerPoczty.Send(wiadomosc);
            wiadomosc.Dispose();
            return true;
        }
    }

}