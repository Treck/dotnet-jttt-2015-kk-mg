﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using System.Net.Mime;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Windows.Forms;

namespace JTTT
{
    public class ListaZadan
    {
        public int listaZadanId { get; set; }
        public virtual BindingList<Warunek> ListaWarunkow { get; set; }

        public ListaZadan()
        {
            ListaWarunkow = new BindingList<Warunek>();
        }

        public void CzyscListe()
        {
            ListaWarunkow.Clear();
        }

        public void ZapisDoBazy()
        {
            using (var ctx = new BazaDbContext())
            {
                foreach (var warunek in ListaWarunkow)
                    ctx.Warunek.Add(warunek);

                ctx.SaveChanges();
            }
        }

        public void OdczytZBazy()
        {
            ListaWarunkow.Clear();

            using (var ctx = new BazaDbContext())
            {
                foreach (var w in ctx.Warunek)
                    ListaWarunkow.Add(w);

            }
        }

        public void CzyscBaze()
        {
            ListaWarunkow.Clear();
            using(var ctx = new BazaDbContext())
            {
                foreach(Warunek warunek in ctx.Warunek)
                {
                    ctx.Warunek.Remove(warunek);
                }
                ctx.SaveChanges();
            }
        }

        public void Serializacja()
        {
            if (ListaWarunkow.Count > 0)
            {
                FileStream fs = new FileStream("DataFile.dat", FileMode.Create);
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    formatter.Serialize(fs, ListaWarunkow);
                }
                catch (SerializationException e)
                {
                    Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                    throw;
                }
                finally
                {
                    fs.Close();
                }
            } 
        }

        public void Deserializacja()
        {
           if (new FileInfo("DataFile.dat").Length != 0)
           {
               FileStream fs = new FileStream("DataFile.dat", FileMode.Open);
               try
               {
                   BinaryFormatter formatter = new BinaryFormatter();
                   ListaWarunkow = (BindingList<Warunek>)formatter.Deserialize(fs);
               }
               catch (SerializationException e)
               {
                   Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                   throw;
               }
               finally
               {
                   fs.Close();
               }
           } 
        }

        public override string ToString()
        {
            return "Lista Zadan\n";
        }
    }
}
