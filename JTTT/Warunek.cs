﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    [Serializable]
    public class Warunek
    {
        public int Id {get; set;}
        public string adresURL { get; set; }
        public string poszukiwaneSlowo { get; set; }
        public string email { get; set; }
        public static int kolejnyNumerPliku {get; set;}
        public virtual ListaZadan ListaZadan { get; set; }

        public Warunek() { }

        public Warunek(string _adresURL = "", string _poszukiwaneSlowo = "", string _email = "")
        {
            adresURL = _adresURL; 
            poszukiwaneSlowo = " " + _poszukiwaneSlowo + " ";
            email = _email;
            kolejnyNumerPliku = 0;
        }

        public override string ToString()
        {
            return "Strona: \"" + adresURL + "\"   Slowo: \"" + poszukiwaneSlowo + "\"   Email: \"" + email + "\"";
        }

        public string Pobierz_strone()
        {
            using (WebClient wc = new WebClient())
            {
                byte[] data = wc.DownloadData(adresURL);
                string html = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));
                return html;
            }
        }

        public HtmlDocument ZaladujStrone()
        {
            HtmlDocument dokumentHTML = new HtmlDocument();
            string pageHtml = Pobierz_strone();
            dokumentHTML.LoadHtml(pageHtml);

            return dokumentHTML;
        }

        public string PobierzAdresObrazka(HtmlNode node)
        {
            return node.GetAttributeValue("src", "");
        }
        public string PobierzOpisObrazka(HtmlNode node)
        {
            return node.GetAttributeValue("alt", "");
        }

        public bool CzyLinkPoprawny(string _link, string _poszukiwaneSlowo)
        {
            if (_link.IndexOf(_poszukiwaneSlowo) != -1)
                return true;
            return false;
        }
        public bool SprawdzPoprawnoscObrazka(HtmlNode node)
        {
            string poprawny_link = "http://";

            if (CzyLinkPoprawny(PobierzOpisObrazka(node), poszukiwaneSlowo))
                if (CzyLinkPoprawny(PobierzAdresObrazka(node), poprawny_link))
                    return true;

            return false;
        }

        public BindingList<String> ZdobadzObrazki()
        {
            HtmlDocument dokumentHTML = ZaladujStrone();
            BindingList<String> PoprawneSciezkiDoObrazkow = new BindingList<string>();

            var nodes = dokumentHTML.DocumentNode.Descendants("img");

            foreach (var node in nodes)
                if (SprawdzPoprawnoscObrazka(node))
                    ZapiszObrazkiNaDysku(PoprawneSciezkiDoObrazkow, node);

            return PoprawneSciezkiDoObrazkow;
        }

        public void ZapiszLinkiDoObrazkow(BindingList<String> Lista, HtmlNode node)
        {
            Lista.Add(PobierzAdresObrazka(node));
        }

        public void ZapiszObrazkiNaDysku(BindingList<String> Lista, HtmlNode node)
        {
            using (WebClient client = new WebClient())
            {
                String LokalizacjaNaDysku = "./obrazek" + kolejnyNumerPliku + ".jpg";
                client.DownloadFile(PobierzAdresObrazka(node), LokalizacjaNaDysku);
                Lista.Add(LokalizacjaNaDysku);
                kolejnyNumerPliku++;
            }
        }

        public void UsunObrazkiZDysku(String Plik)
        {
            System.IO.File.Delete(Plik);
        }
    }
}
