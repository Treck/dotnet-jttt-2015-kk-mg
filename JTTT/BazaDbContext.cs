﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class BazaDbContext : DbContext
    {
        public BazaDbContext() : base("BazaWarunkow")
        {
            Database.SetInitializer<BazaDbContext>(new BazaDbInitializer());
        } 

        public DbSet<Warunek> Warunek { get; set; }
    }
}
