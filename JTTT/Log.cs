﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class Log
    {
        public void NowyWpis(string _informacja)
        {
            using (StreamWriter w = new StreamWriter("jttt.log", true))
            {
                w.WriteLine("Czas: " + DateTime.Now.ToString());
                w.WriteLine("Operacja: " + _informacja);
                w.WriteLine("----------------------");
                w.WriteLine();
            }
        }
    }
}
